import re
import sys
import os 


def decrypt(data):
   converted_data = list(data)
   i = 0
   while i < (len(converted_data) - 1):
       if converted_data[i] == converted_data[i+1]: # checking equality of pair symbols
           converted_data.pop(i) # delete first symbol
           converted_data.pop(i) # delete second symbol(after deleting first, second symbol shifts to first place)
           if i != 0: # if symbols were deleted roll back to check shifted list form scratch
               i -= 1
           continue
       else: # if pair not equal move to next symbol
            i += 1
   return "".join(converted_data)


def file_checking_and_reading():
    if len(sys.argv) == 2: # checking if file provided
        data_file = sys.argv[1]
    else:
        print('no file provided!')
        sys.exit()

    with open(data_file, "r") as data:
        encrypted_data = data.read() # reading data from file
        print(os.path.basename(data_file) + ":" + encrypted_data)
    return encrypted_data


encrypted_data_form_file = file_checking_and_reading()
decrypted_data = decrypt(encrypted_data_form_file)
print("Output:", decrypted_data)
